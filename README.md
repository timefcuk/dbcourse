# Курс по базам данных для первого семестра
![Alt text](/course_image.jpg "Да-да, вы не ошиблись, этот хычиновый стек отождествляет собой базу данных")
## Progress

### TODO:
* Сделать четкие требования к каждой лабе
* Сделать к каждой лабе по одному отчету

### DONE:
* 1 ПЗ
* 2 ПЗ
* 3 ПЗ
* 4 ПЗ
* 5 ПЗ
* 6 ПЗ
* 1 Лекция
* 2 Лекция 
* 3 Лекция
* 4 Лекция
* 5 Лекция
* 7 Лекция
* 8 Лекция
* 9 Лекция

## Notes

* How to import dump into the database
`echo .read dump_file.sql|sqlite3 database_file.db`

## Resources

* http://www.sqltutorial.org/sql-sample-database/
* http://citforum.ru/database/dblearn/index.shtml
* https://lagunita.stanford.edu/courses/Engineering/db/2014_1/info
* http://www.databasejournal.com/scripts/practice-sql.html
* http://chinookdatabase.codeplex.com/


