CREATE TABLE zipcodes (
  zip integer(5) primary key,
  city  varchar(30),
  State varchar(20));
CREATE TABLE employees (
  eno  varchar(10) primary key,
  ename  varchar(30),
  zip  integer(5) references zipcodes(zip),
  hire_date date);
CREATE TABLE books (
  bno  integer(5) primary key,
  bname  varchar(30),
  qoh  integer(5) not null,
  price  dec(6,2) not null);
CREATE TABLE customers (
  cno   integer(5) primary key,
  cname  varchar(30),
  street varchar(30),
  zip  integer(5)  references zipcodes(zip),
  phone  char(12));
CREATE TABLE orders (
  ono  integer(5)  primary key,
  cno  integer(5)  references customers(cno),
  eno  varchar(10)  references employees(eno),
  received date,
  shipped date
);
CREATE TABLE odetails (
  ono  integer(5)  references orders(ono),
  bno  integer(5)  references books(bno),
  quantity integer(10) not null,
  primary key (ono, bno));

